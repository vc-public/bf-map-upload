# Problem: 40+ people with video footage. How do we get video footage to the editors?

At the time (2017) this was my solution. A front-end only (no server) file uploader to S3. Using fine uploader this would allow anyone with a Google account to put files into the s3 bucket. This worked by requiring the user to login to Google. Then the upload option would become visible and using the OAuth credentials the user would be generated temporary AWS access keys and ID for uploading (this is completely transparent to the user) it just works for them. You can read more about it in the pdf (Uploads without any server code by Ray Nicholus Fine Uploader.pdf)

Today (2023) I would probably just use a WebRTC p2p file transfer service. Anyone who has a file to upload would just give me the link to download. (Of course, I would only click links from people I trust). This solution doesn’t require a backend server either like the above solution and is probably a lot similar and more cost effective since you don’t have to pay AWS fees. 

Anyways this is code from 2017, the front end holds up well, but since FineUploader has been archived since 2018, I doubt this code will still work. Don’t ask more for help with it either :)

cya
